<?php


class BinarySearchClass
{
    // PHP program to implement
    // recursive Binary Search

    // A recursive binary search
    // function. It returns location
    // of x in given array arr[l..r]
    // is present, otherwise -1
    function binarySearch($arr, $l, $r, $x)
    {
        if ($r >= $l) {
            $mid = ceil($l + ($r - $l) / 2);

            // If the element is present
            // at the middle itself
            if ($arr[$mid] == $x)
                return floor($mid);

            // If element is smaller than
            // mid, then it can only be
            // present in left subarray
            if ($arr[$mid] > $x)
                return $this->binarySearch($arr, $l,
                    $mid - 1, $x);

            // Else the element can only
            // be present in right subarray
            return $this->binarySearch($arr, $mid + 1,
                $r, $x);
        }

        // We reach here when element
        // is not present in array
        return -1;
    }

    // A iterative binary search
    // function. It returns location
    // of x in given array arr[l..r]
    // if present, otherwise -1
    function iterativeBinarySearch($arr, $l, $r, $x)
    {
        while ($l <= $r)
        {
            $m = $l + ($r - $l) / 2;

            // Check if x is present at mid
            if ($arr[$m] == $x)
                return floor($m);

            // If x greater, ignore
            // left half
            if ($arr[$m] < $x)
                $l = $m + 1;

            // If x is smaller,
            // ignore right half
            else
                $r = $m - 1;
        }

        // if we reach here, then
        // element was not present
        return -1;
    }
}

$search = new BinarySearchClass();

// Recursive implementation of recursive Binary Search
$arr = array(2, 3, 4, 10, 40);
$n = count($arr);
$x = 10;
$result = $search->binarySearch($arr, 0, $n - 1, $x);
if (($result == -1))
    echo "Element is not present in array" . PHP_EOL;
else
    echo "Element is present at index " . $result . PHP_EOL;

// Recursive implementation of iterative Binary Search
$arr = array(2, 3, 4, 10, 40);
$n = count($arr);
$x = 10;
$result = $search->iterativeBinarySearch($arr, 0, $n - 1, $x);
if(($result == -1))
    echo "Element is not present in array" . PHP_EOL;
else
    echo "Element is present at index " . $result . PHP_EOL;